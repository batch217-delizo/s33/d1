//console.log("Hello World");

//Section - JavaScript Synchronous vs Asynchronous
//JS --> Synchronous - it can execute one statement or one line of code at a time.

console.log("Hello World");
console.log("Hello Again!");
console.log("Goodbye");

console.log("Hello World!");
/* 
for(let i = 0; i <= 1500; i++) {
  console.log(i);
}
 */
console.log("Hello Again!");


//Section - Greeting all post
//fetch() - fetch request
//The fetch API allows programmers to assynchronously request for a resource (data).

/* 

  Syntax -- //fetch()
  console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

*/

fetch("https://jsonplaceholder.typicode.com/posts")
  //We use "json" method from the response object to covert data to jsn format
.then((response) =>  response.json())
.then((data) => console.log(data));

//The "async" and "await" keywords is another approach that can be used to achieve asynchronous
//Used in function to indicate which portions of the code should be awaited for

async function fetchData() {
  let result = await fetch("https://jsonplaceholder.typicode.com/posts")
  
  // result returned "response" --> expecting a promise
  console.log(result);
  console.log(typeof result);
  console.log(result.body);
  
  let json = await result.json();
  console.log(json);
}

fetchData();

//Section Getting a specific post

fetch("https://jsonplaceholder.typicode.com/posts")
  .then((response) => response.json())
  .then((json) => console.log("last" + json) + " the last data");

//SECTION Creating a post

/* 

  SYNTAX :

  fetch("URL", options)
  .then((response) => {})
  .then((response) => {})

*/

fetch("https://jsonplaceholder.typicode.com/posts", {
   method: "POST",
   headers: {
      "Content-Type" : "application/json"
   },
   body: JSON.stringify({
      title: "New Post",
      body: "Hello World",
      userID: 1
   })
})
.then((response) => response.json())
.then((json) => console.log(json))


//SECTION - Updating a post
//PUT - used to update the whole document

fetch("https://jsonplaceholder.typicode.com/posts/1",{
  method: "PUT",
  headers: {
     "Content-Type" : "application/json"
  },
  body: JSON.stringify({
     id:  1,
     title: "Updated Post",
     body: "Hello Again",
     userId: 1,
  })
})
.then( response => response.json())
.then( json => console.log(json)) 


//Section - Patch method
//Update a specific post, specific property in a post

fetch("https://jsonplaceholder.typicode.com/posts/1",{
  method: "PATCH",
  headers: {
     "Content-Type" : "application/json"
  },
  body: JSON.stringify({
     title: "Corrected Post"
  })
})
.then( response => response.json())
.then( json => console.log(json)) 


//Section Deleting a post
fetch("https://jsonplaceholder.typicode.com/posts/1",{
  method: "DELETE"
})


//SECTION Filtering Post
/* 
  SYNTAX 
  Individual Parameter
  //'URL?parameter=value'
  Multiple Parameter
  //url?paramA=valueA&paramB=valueB
*/

fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
.then((response) => response.json())
.then((json) => console.log(json))

//SECTION Retrieving comment is a specific Post
fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then((response) => response.json())
.then((json) => console.log(json))




















































